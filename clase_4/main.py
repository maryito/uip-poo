from clase_4.calendario import Calendario, Evento

def menu():
    print("---- MENU DE INICIO ----")
    print("1. AGREGAR")
    print("2. VER")
    print("3. ELIMINAR")
    print("4. SALIR")
    opc = int( input(("\nSeleccion una opcion ==> ")))
    return opc

def menu_entidad():
    print("******* MENU DE ENTIDAD *******")
    print("1. CALENDARIO")
    print("2. EVENTO")
    print("3. USUARIO")
    opc2 = int(input("\nSeleccion una opcion ==> "))
    return opc2

if __name__ == '__main__':
    print("Clase #4 = Calendario = \n")

    # # instancia
    # escolar = Calendario("Escolar")
    # print(escolar.nombre, escolar.id, escolar.color)
    #
    # escolar.color = "Rojo"
    # print(escolar.color)
    #
    # print("Total en la lista", len(escolar.lista))
    # for x in range(0,1):
    #     print("Capturado la lista # ", str(x + 1))
    #     temp =  input("Lista de deseo: ")
    #     # Agregado el campo a la lista
    #     escolar.lista.append(temp)
    #
    # print("Total en la lista", len(escolar.lista))
    # print("\n Lista del calendario es: ", escolar.lista)
    #
    # # Instancia #2
    # universidad = Calendario("Universidad")
    # print(universidad.nombre, universidad.id)
    # universidad.lista.append("Cursos")
    # universidad.lista.append("Parciales")
    # universidad.lista.append("Actividades")
    # print(universidad.lista)

    # INstancia de la clase Evento
    # graduacion = Evento()
    # print("ID del evento es ==> ", graduacion.id)
    # print(graduacion.getNombre())
    #
    # nom_grad = input("Ingresa el nombre del evento: ")
    # graduacion.setNombre("")
    # graduacion.getNombre()

    # Inicializando variable
    nuevo_cal = ""
    nuevo_evt = ""
    nuevo_usr = ""
    control = 0
    print(type(nuevo_cal))


    while control != 1:
        # Instancia con menu
        seleccion = menu()
        if seleccion == 1:
            """ AGREGAR"""
            sel_entidad = menu_entidad()
            if sel_entidad == 1:
                """ Calendario"""
                nom = input("Ingrese el nombre del calendario: ")
                nuevo_cal = Calendario(nom)
                nuevo_cal.color = input("Ingrese el color ===> ")
                nuevo_cal.lista.append("Personal")
                nuevo_cal.lista.append("Trabajo")

            elif sel_entidad == 2:
                """ Evento"""
                nuevo_evt = Evento()
                nom = input("Ingrese el nombre del evento: ")
                inicio = input("Ingrese la Fecha de INICIO ==> ")
                f_fin = input("Ingrese la Fecha de Finalizacion ==> ")
                detalle = input("Desciption del evento ===> ")

                # Agrega un evento a tarves del metodo
                nuevo_evt.agregar(nom, inicio, f_fin, detalle)

            elif sel_entidad == 3:
                """ Usuario"""
                pass
            else:
                print("Opcion invalidad")
        elif seleccion == 2:
            """ VER """
            sel_entidad = menu_entidad()
            if sel_entidad == 1:
                """ Calendario"""
                nuevo_cal.ver()
            elif sel_entidad == 2:
                """ Evento"""
                nuevo_evt.ver()
            elif sel_entidad == 3:
                """ Usuario"""
                pass
            else:
                print("Opcion invalidad")
        elif seleccion == 3:
            """ ELIMINAR"""
            pass
        elif seleccion == 4:
            print("¿Seguro que desea salir del sistema 1=SI y 0=NO?")
            control = int(input(''))
        else:
            print("Opcion invalida!!!")





